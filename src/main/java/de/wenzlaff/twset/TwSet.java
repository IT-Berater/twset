package de.wenzlaff.twset;

import static de.wenzlaff.twset.Wert.EINFACH;
import static de.wenzlaff.twset.Wert.MITTEL;
import static de.wenzlaff.twset.Wert.SCHWIERIG;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.store.storage.embedded.types.EmbeddedStorage;
import org.eclipse.store.storage.embedded.types.EmbeddedStorageManager;
import org.eclipse.store.storage.restservice.types.StorageRestService;
import org.eclipse.store.storage.restservice.types.StorageRestServiceResolver;

/**
 * @author Thomas Wenzlaff
 */
public class TwSet {

	private static List<Task> alleTasks;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		EmbeddedStorageManager storage = EmbeddedStorage.start();

		if (storage.root() == null) {
			List<Task> tasks = getInitTasks();
			System.out.println("Keine DB vorhanden, anlegen in ../storage Verzeichnis ...");
			storage.setRoot(tasks);
			storage.storeRoot();
			alleTasks = tasks;
		} else {
			System.out.println("DB vorhanden, verwenden ...");
			alleTasks = (List<Task>) storage.root();
		}

		// Priorisierung nach SET-Methode
		List<Task> simpleTasks = new ArrayList<>();
		List<Task> easyAndValuableTasks = new ArrayList<>();

		for (Task task : alleTasks) {
			if (task.getComplexity() == EINFACH) {
				simpleTasks.add(task);
			}
			if (task.getEffort() == EINFACH && task.getValue() == SCHWIERIG) {
				easyAndValuableTasks.add(task);
			}
		}

		// Ausgabe der priorisierten Aufgaben
		System.out.println("Einfache Aufgaben:");
		for (Task task : simpleTasks) {
			System.out.println("- " + task.getDescription());
		}

		System.out.println("\nLeichte und wertvolle Aufgaben:");
		for (Task task : easyAndValuableTasks) {
			System.out.println("- " + task.getDescription());
		}

		// und eine REST-Service starten ... http://localhost:4567/store-data/root
		StorageRestService service = StorageRestServiceResolver.resolve(storage);
		service.start();
	}

	private static List<Task> getInitTasks() {
		// Erstellen einer Liste von Aufgaben
		List<Task> tasks = new ArrayList<>();
		tasks.add(new Task("Bugfix für Anmeldeformular", EINFACH, MITTEL, SCHWIERIG));
		tasks.add(new Task("Implementierung neuer Funktion", SCHWIERIG, SCHWIERIG, MITTEL));
		tasks.add(new Task("Aktualisierung von Bibliotheken", EINFACH, EINFACH, EINFACH));
		tasks.add(new Task("Optimierung der Datenbankabfragen", SCHWIERIG, MITTEL, SCHWIERIG));
		tasks.add(new Task("Dokumentation für API schreiben", MITTEL, EINFACH, SCHWIERIG));
		return tasks;
	}
}
