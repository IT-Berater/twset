package de.wenzlaff.twset;

/**
 * @author Thomas Wenzlaff
 */
public class Task {

	private String description;
	private Wert complexity; // 0 für einfach, 1 für mittel, 2 für schwierig
	private Wert effort; // 0 für geringer Aufwand, 1 für mittlerer Aufwand, 2 für hoher Aufwand
	private Wert value; // 0 für niedriger Wert, 1 für mittleren Wert, 2 für hohen Wert

	/**
	 * 
	 * @param description
	 * @param complexity
	 * @param effort
	 * @param value
	 */
	public Task(String description, Wert complexity, Wert effort, Wert value) {
		this.description = description;
		this.complexity = complexity;
		this.effort = effort;
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public Wert getComplexity() {
		return complexity;
	}

	public Wert getEffort() {
		return effort;
	}

	public Wert getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "Task [description=" + description + ", complexity=" + complexity + ", effort=" + effort + ", value="
				+ value + "]";
	}
}
